%global _empty_manifest_terminate_build 0
Name:           python-click
Version:        7.1.2
Release:        1
Summary:        Composable command line interface toolkit
License:        BSD
URL:            https://palletsprojects.com/p/click/
Source0:        https://files.pythonhosted.org/packages/27/6f/be940c8b1f1d69daceeb0032fee6c34d7bd70e3e649ccac0951500b4720e/click-7.1.2.tar.gz
BuildArch:      noarch
%description
Click is a Python package for creating beautiful command line interfaces
in a composable way with as little code as necessary. It's the
"Command Line Interface Creation Kit". It's highly configurable but comes
with sensible defaults out of the box.

%package -n python3-click
Summary:        Composable command line interface toolkit
Provides:       python-click
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
%description -n python3-click
Click is a Python package for creating beautiful command line interfaces
in a composable way with as little code as necessary. It's the
"Command Line Interface Creation Kit". It's highly configurable but comes
with sensible defaults out of the box.

%package help
Summary:        Composable command line interface toolkit
Provides:       python3-click-doc
%description help
Click is a Python package for creating beautiful command line interfaces
in a composable way with as little code as necessary. It's the
"Command Line Interface Creation Kit". It's highly configurable but comes
with sensible defaults out of the box.

%prep
%autosetup -n click-%{version}

%build
%py3_build


%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%check
%{__python3} setup.py test

%files -n python3-click -f filelist.lst
%dir %{python3_sitelib}/*


%files help -f doclist.lst
%{_docdir}/*

%changelog
* Tue Jul 13 2021 OpenStack_SIG <openstack@openeuler.org> - 7.1.2-1
- Upgrade to version 7.1.2

* Wed Oct 21 2020 chengzihan <chengzihan2@huawei.com> - 7.0-3
- Remove subpackage python2-click

* Thu Sep 10 2020 liuweibo <liuweibo10@huawei.com> - 7.0-2
- Fix Source0

* Fri Nov 1 2019 shanshishi <shanshishi@huawei.com> - 7.0-1
- Init package
